import React from 'react';
import ImgBg from '../images/knm.jpg'
import styled from 'styled-components'
import Typewriter from 'typewriter-effect'
import ScrollDown from './scrolldown'
import Fade from 'react-reveal/Fade'

const HeroContainer = styled.div`
  background: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.1)),
    url(${ImgBg});
  height: 100vh;
  background-position: center;
  background-size: cover;
`;

const HeroItems = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  max-height: 100%;
  width: 100%;
  color: #fff;
  line-height: 1.5;
  font-weight: bold;
  letter-spacing: 5px;
  

  @media screen and (max-width: 650px) {
    width: 100%;
  }
`;

const HeroH1 = styled.h1`
position: relative;
z-index: 10;
display: flex;
flex-direction: column;
max-width: 1600px;
width: calc(100% - 100px);
color: #fff;
align-items: center;
font-family: 'Montserrat', sans-serif;
font-size: clamp(1.5rem, 6vw, 20rem);

h1{
    font-size: clamp(3rem, 40vw, 40rem);
    font-weight: 400;
    text-shadow: 0px 0px 20px rgba(0,0,0,0.4);
    text-align: left;
    margin-bottom: 0.8rem;
    -webkit-animation: mover 3s alternate;
    animation: mvoer 3s alternate;

}

p{
    margin-bottom: 1.3rem;
    text-shadow: 0px 0px 20px rgba(0,0,0,0.4);
}

@-webkit-keyframes mover {
    0% { transform: translateY(0);}
    100%{ transform: translateY(-20px)}
}
`;

const HeroP = styled.p`
  font-size: clamp(1rem, 2vw, 3rem);
  margin-bottom: 2rem;
  font-family: 'Montserrat', sans-serif;
`;


const Hero = () => {
  return (
    <HeroContainer id='home'>

      <HeroItems>

        <HeroH1><Fade left>Hi, I am</Fade></HeroH1>
        <HeroH1><Fade right> Kyaw Naing Moe</Fade></HeroH1>

        <HeroP><Typewriter
          options={{
            strings: ["Web Developer", "Coach", "Learner"],
            autoStart: true,
            loop: true,
          }}
        /></HeroP>
      </HeroItems>
      <a href='#about'><ScrollDown /></a>
    </HeroContainer>
  );
};

export default Hero;
