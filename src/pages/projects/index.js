import React from 'react'
import { Card } from 'antd';
import { Timeline, Events, UrlButton, ImageEvent, TextEvent } from '@merc/react-timeline'
import { ClockCircleOutlined } from '@ant-design/icons'
import './projects.css'
import { Collapse } from 'antd';

import HTML5 from '../../images/html-5.svg'
import CSS3 from '../../images/css3.svg'
import REACT from '../../images/react.svg'
import ANTD from '../../images/antd.png'
import GIT from '../../images/gitlab.png'
import my from '../../images/knmcu-2.jpg'
import Knex from '../../images/knex.png'
import Koa from '../../images/koa.jpg'
import Netlify from '../../images/netlify.png'

const { Panel } = Collapse;

const Projects = () => {



  return (
    <div id='projects' >
      <h1 className='project-title'>PROJECTS</h1>
      <div className='projects'>
        <Timeline>
          <Events>
            <ImageEvent date="Elections Department Sagaing" src='https://scontent.frgn1-2.fna.fbcdn.net/v/t1.0-9/s960x960/136183375_1073326693079960_3570286454259206782_o.jpg?_nc_cat=108&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFmUq1LP6MajRujarcj-gI38cJ6ZMDgJRXxwnpkwOAlFfWtm49eabDu2QihQgqpAESusOgzM10XKmXi5sigmv13&_nc_ohc=jpADreVekzQAX8J5occ&_nc_ht=scontent.frgn1-2.fna&tp=7&oh=ba505a63c89ae444671e25261d5352c1&oe=601A28C3' className="pj-event" alt="Election Department Sagaing">
              <div className="d-flex justify-content-between flex-column mt-1">
                <div>
                  <Collapse accordion >
                    <Panel header={<h3 style={{ textAlign: 'center' }}>SEE PROJECT DETAIL</h3>} key="1" showArrow={false} className='panel'>
                      <p><strong>Description:</strong> မဲဆန္ဒရှင်ဆိုင်ရာ၊ လွတ်တော်ကိုယ်စားလှယ်ဆိုင်ရာ၊ မဲဆန္ဒနယ်ဆိုင်ရာ၊ နိုင်ငံရေးပါတီဆိုင်ရာ အချက်အလက်တို့ကို ကွန်ပျူတာစနစ်တွင် မွန်းမံပြင်ဆင် လုပ်ဆောင်ထားခြင်းဖြစ်ပါသည်။</p>
                      <hr />
                      <strong>Features:</strong>
                      <p>Election နဲ့ပတ်သက်ပြီး မင်းကြိုက်တာရှာကွာ။ ရှိရင် ကျလာလိမ့်မယ် 🤪။</p>
                      <hr />

                      <strong>Tech used:</strong>
                      <p className='tech-use'>
                        <ul>
                          <li>
                            <span className="item">
                              <img src={HTML5} alt="HTML 5" rounded className="image-style m-1"></img> HTML5
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={CSS3} alt="CSS 3" rounded className="image-style m-1"></img> CSS3
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={REACT} alt="React" rounded className="image-style1 m-1"></img> React
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={ANTD} alt="Antd Desing" rounded className="image-style1 m-1"></img> Antd Design
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={Koa} alt="Koa" rounded className="image-style1 m-1"></img> Koa Framework
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={Knex} alt="Knex" rounded className="image-style1 m-1"></img> Knex.JS
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={Netlify} alt="Netlify" rounded className="image-style1 m-1"></img> Netlify
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={GIT} alt="Github" rounded className="image-style1 m-1"></img> GitLab
                            </span>
                          </li>
                        </ul>
                      </p>
                    </Panel>
                  </Collapse>
                  <div className="url-btn">
                    <UrlButton href="https://let-vote.netlify.app/">
                      SEE LIVE
                    </UrlButton>
                    <UrlButton href="https://gitlab.com/ucsm-team-v/let-vote" >
                      SOURCE CODE
                    </UrlButton>
                  </div>
                </div>
              </div>
            </ImageEvent>
            <ImageEvent date="My Portfolio" src='https://scontent.frgn1-2.fna.fbcdn.net/v/t1.0-9/134931731_1073848116361151_4866109720203506359_o.jpg?_nc_cat=104&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeHvK6U02ntNmhsvxff-DygA7PrC3P08TTPs-sLc_TxNM0nn6v7YxNFAA4HDN7kLph4Z_ERQwtmymz6DWW0dmMjq&_nc_ohc=lPOet90QmycAX9CKgKr&_nc_ht=scontent.frgn1-2.fna&oh=4bc6e3ad78deb3cd5dc46b80c8744844&oe=601D386C' className="pj-event" alt="Election Department Sagaing">
              <div className="d-flex justify-content-between flex-column mt-1">
                <div>
                  <Collapse accordion >
                    <Panel header={<h3 style={{ color: '#ffff', textAlign: 'center' }}>SEE PROJECT DETAIL</h3>} key="1" showArrow={false} className='panel'>
                      <p><strong>Description:</strong> ဘာရယ်မဟုတ်ဘူး ပျင်းလို့လျောက်လုပ်ထားတာ။ ဒါနဲ့ ကျနော် Single ပါဗျ။ Crush လို့‌ေတာ့ရတယ်။ ချစ်လို့တော့မရဘူးနော်😂။</p>
                      <hr />
                      <strong>Features:</strong>
                      <p>လာပါ မမ။ ကျနော်နဲ့တွဲရအောင် 😔။</p>
                      <hr />

                      <strong>Tech used:</strong>
                      <p className='tech-use'>
                        <ul>
                          <li>
                            <span className="item">
                              <img src={HTML5} alt="HTML 5" rounded className="image-style m-1"></img> HTML5
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={CSS3} alt="CSS 3" rounded className="image-style m-1"></img> CSS3
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={REACT} alt="React" rounded className="image-style1 m-1"></img> React
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={ANTD} alt="Antd Desing" rounded className="image-style1 m-1"></img> Antd Design
                            </span>
                          </li>
                          <li>
                            <span className="item">
                              <img src={GIT} alt="Github API" rounded className="image-style1 m-1"></img> GitLab API
                            </span>
                          </li>
                        </ul>
                      </p>
                    </Panel>
                  </Collapse>
                  <div className="url-btn">
                    <UrlButton href="https://kyawnaingmoe.netlify.app/">
                      SEE LIVE
                    </UrlButton>
                    <UrlButton href="https://gitlab.com/ucsm-team-v/let-vote" >
                      SOURCE CODE
                    </UrlButton>
                  </div>
                </div>
              </div>
            </ImageEvent>
          </Events>
        </Timeline>
      </div>
    </div>
  )
};

export default Projects;