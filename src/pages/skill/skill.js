import React from 'react'
import { Divider, Row, Col } from 'antd'
import { Space } from 'antd'
import './skill.css'

import { skills } from './skilldata.js'
const Skill = () => {

  return (
    <div id="skills">
       <h1 className='skill-title'>TECH SKILLS</h1>
      <div className='skill-container'>
        
            <div className='skill-col-left'>
            <div className='card-style'>
              <Divider orientation='center'><text className='skill-text'>FRONTED</text></Divider>
              {skills.frontend.map((skill, index) => (
                <span className="p-2" key={`${skill.skillName}${index}`}>
                  <a className="text-dark text-decoration-none" href={skill.link} target="_blank" rel="noopener noreferrer">
                    <img src={skill.imgSrc} alt={skill.imgAltText} rounded className="image-style m-1" style={{ width: '2rem' }}></img>&nbsp;&nbsp;
                        {skill.skillName}
                  </a>
                </span>
              ))}</div>
          </div>
         <div className='skill-col-right'>
            <div className='card-style'>
              <Divider orientation='center'><text className='skill-text'>BACKEND</text></Divider>
              {skills.backend.map((skill, index) => (
                <span className="p-2" key={index}>
                  <a className="text-dark text-decoration-none" href={skill.link} target="_blank" rel="noopener noreferrer">
                    <img src={skill.imgSrc} alt={skill.imgAltText} rounded className="image-style m-1"></img> &nbsp;&nbsp; {skill.skillName}
                  </a>
                </span>
              ))}

              <Divider orientation='center'><text className='skill-text'>DATABASE</text></Divider>
              {skills.databases.map((skill, index) => (
                <span className="p-2" key={index}>
                  <a className="text-dark text-decoration-none" href={skill.link} target="_blank" rel="noopener noreferrer">
                    <img src={skill.imgSrc} alt={skill.imgAltText} rounded className="image-style m-1"></img> &nbsp;&nbsp; {skill.skillName}
                  </a>
                </span>
              ))}
            </div>
            </div>
          <di className='skill-bot'>
            <div className='card-style'>
              <Divider orientation='center'><text className='skill-text'>HOSTING</text></Divider>
              {skills.hostingPlatforms.map((skill, index) => (
                <span className="p-2" key={index}>
                  <a className="text-dark text-decoration-none" href={skill.link} target="_blank" rel="noopener noreferrer">
                    <img src={skill.imgSrc} alt={skill.imgAltText} rounded className="image-style"></img> &nbsp;&nbsp; {skill.skillName}
                  </a>
                </span>
              ))}
              </div>
              </di>
          

        

      </div>
      
    </div>
  )
}

export default Skill
