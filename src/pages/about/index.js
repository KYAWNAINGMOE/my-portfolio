import React from 'react'
import styled from "styled-components";
import Fade from 'react-reveal/Fade'
import image from '../../images/knmcu-2.jpg'
import { Divider } from 'antd'

const Section = styled.section`
    width: 100%;
    height: 600px;
    padding: 5.4rem 0rem;
    font-family: 'Montserrat', sans-serif;
    background: #2F4E68;

    h1{
        font-family: 'Montserrat', sans-serif;
        color: #fff;
        letter-spacing: 2px;
    }
    
    @media screen and (max-width: 768px){
        height: 100%;
    }
`;

const Container = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 700px;

    @media screen and (max-width: 768px){
        grid-template-columns: 1fr;
        grid-template-rows: 500px;
    }

`;

const ColumnLeft = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding: 4rem 2rem;
    

    p{
        font-family: 'Montserrat', sans-serif;
        letter-spacing: 1px;
        font-size: clamp(1rem, 2vw, 1rem);
        color: #ffff;
    }
    
    @media screen and (max-width: 768px){
        padding: 0rem 2rem;
    }
`;

const ColumnRight = styled.div`
    display: flex;
    justify-content: center;
    padding: 3rem 2rem;

    img{
      width: 80%;
        height: 60%;
        object-fit: cover;
        border-radius: 30% 70% 70% 30% / 30% 30% 70% 70%;
        

        @media screen and (max-width: 768px){
            width: 90%;
            height: 90%;
            object-fit: cover;
        }
    }
`;
const InfoSection =() =>{
    return (
        <Section id='about'>
          <h1 style={{textAlign: 'center'}}>ABOUT ME</h1>
            <Container>
            <ColumnRight >
            
                <Fade left>
                <img src={image} /> 
                </Fade>
                
                </ColumnRight>
                <ColumnLeft>
                <p>Hi there! I am <strong>&nbsp;Kyaw Naing Moe</strong>
                <br />A passionate programmer and a Coach, born and brought up in Myanmar. I am a Full Stack Web Developer with React.js, Redux, Express.js, Node.js, Koa and PostgreSQL as my tech stack.
                <br />
                In 2021, I successfully completed my Bachelor's degree with specialization in 'Computer Science' from the <a style={{textDecoration: 'none', color: '#84BD26', fontSize: '15px'}} href='https://ucsmonywa.edu.mm/'><strong>University of Computer Studies Monywa</strong></a>.
                <br />
                Working with the clients, my goal is always driven towards providing amazing experience with the best level of quality and service to them.
                <br />
                Along with that, I also help people as a COACH on their journey of becoming a professional programmer. I love learning about new technologies, what problems are they solving and How can I use them to build better and scalable products.
                <br /> <br /></p>
                
                
                </ColumnLeft>
                
            </Container>
        </Section>
    )
}

export default InfoSection
