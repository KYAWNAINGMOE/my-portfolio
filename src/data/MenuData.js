export const MenuData = [
    {title: 'About', href: '#about',},
    {title: 'Home', link: '/home',},
    {title: 'Skills', link: '/skills',},
    {title: 'Projects', link: '/projects',},
    {title: 'Contact', link: '/contact',},
]