import React from 'react'

import Hero from '../src/components/HeroElement'
import Fade from "react-reveal/Fade";
import DropDown from '../src/components/DropDown'
import {
  BrowserRouter as Router,
} from "react-router-dom";
import './App.css';
import 'antd/dist/antd.css';
import About from '../src/pages/about/index'
import Skill from '../src/pages/skill/skill'
import Projects from '../src/pages/projects/index'
import Contact from '../src/pages/contact/index'

function App() {


  return (
    <div >
      <Router >
      {/* <Navbar /> */}
      <DropDown />
      <Hero />
      <Fade bottom>
        <About />
      </Fade>
      <Fade bottom>
        <Skill />
      </Fade>
      <Fade bottom>
        <Projects />
      </Fade>
      <Fade bottom>
        <Contact />
      </Fade>
      </Router>
    </div>
  );
}

export default App;
